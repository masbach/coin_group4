/////////////// This is a modified version of cubernets' TiktokCommentScraper ///////////////
// https://github.com/cubernetes/TikTokCommentScraper

with ({}) (async function() {

    var AllCommentsXPath                 = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[3]/div[2]';
    var level2CommentsXPath              = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[3]/div[2]/div/div[2]/div/a';

    var publisherProfileUrlXPath         = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/div/a[1]';
    var nicknameAndTimePublishedAgoXPath = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/div/a[2]/span[2]';

    var likeCountXPath                   = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/button[1]/strong';
    var descriptionXPath                 = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/div';
    var tiktokNumberOfCommentsXPath      = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/button[2]/strong';

    var readMoreDivXPath                 = '/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[3]/div[2]/div/div[2]/div/p[1]/text()/..';


    // more reliable than querySelector
    function getElementsByXPath(xpath, parent)
    {
        let results = [];
        let query = document.evaluate(xpath, parent || document,
            null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (let i = 0, length = query.snapshotLength; i < length; ++i) {
            results.push(query.snapshotItem(i));
        }
        return results;
    }

    function getAllComments(){
        return getElementsByXPath(AllCommentsXPath)[0].children;
    }

    function quoteString(s) {
        return '"' + String(s).replaceAll('"', '""') + '"';
    }

    function formatDate(strDate) {
        if (typeof strDate !== 'undefined' && strDate !== null) {
            f = strDate.split('-');
            if (f.length == 1) {
                return strDate;
            } else if (f.length == 2) {
                return f[1] + '-' + f[0] + '-' + (new Date().getFullYear());
            } else if (f.length == 3) {
                return f[2] + '-' + f[1] + '-' + f[0];
            } else {
                return 'Malformed Date';
            }
        } else {
            return 'No date';
        }
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function csvFromComment(comment) {
        nickname = getElementsByXPath('./div[1]/a', comment)[0].outerText;
        user = getElementsByXPath('./a', comment)[0]['href'].split('?')[0].split('/')[3].slice(1);
        commentText = getElementsByXPath('./div[1]/p', comment)[0].outerText;
        timeCommentedAgo = formatDate(getElementsByXPath('./div[1]/p[2]/span', comment)[0].outerText);
        commentLikesCount = getElementsByXPath('./div[2]', comment)[0].outerText;
        //pic = getElementsByXPath('./a/span/img', comment)[0]['src'];
        return quoteString(nickname) + ',' + quoteString(user) + ',' + 'https://example.com' + ',' + quoteString(commentText) + ',' + timeCommentedAgo + ',' + commentLikesCount + ',' + 'quoteString(pic)';
    }

    //set var after 900sec to false
    setTimeout(function () {
        keepCalling = false;
    }, 300000);

    // Loading 1st level comments
    var loadingCommentsBuffer = 30; // increase buffer if loading comments takes long and the loop break too soon
    var numOfcommentsBeforeScroll = getAllComments().length;
    while ((loadingCommentsBuffer > 0)&(keepCalling == true)) {

        allComments = getAllComments();
        lastComment = allComments[allComments.length - 1];
        // Scroll last element into view = scrolling to bottom of comment page
        lastComment.scrollIntoView({block: "end", behavior: "smooth"});

        numOfcommentsAftScroll = getAllComments().length;

        // If number of comments doesn't change after 15 iterations, break the loop.
        if (numOfcommentsAftScroll !== numOfcommentsBeforeScroll) {
            loadingCommentsBuffer = 15;
        } else {
            loadingCommentsBuffer--;
        };
        numOfcommentsBeforeScroll = numOfcommentsAftScroll;
        console.log('Loading 1st level comment number ' + numOfcommentsAftScroll);

        // Wait 0.3 seconds.
        await new Promise(r => setTimeout(r, 300));
        if (numOfcommentsBeforeScroll > 500){
            console.log('500 comments reached. Break the loop.')
            break
        }
        console.log('Buffer ' + loadingCommentsBuffer);
    }
    console.log('Openend all 1st level comments');


    var previousbuffer;
    var iteration45 = 0;

    // Loading 2nd level comments
    loadingCommentsBuffer = 5; // increase buffer if loading comments takes long and the loop break too soon
    while (loadingCommentsBuffer > 0) {
        readMoreDivs = getElementsByXPath(readMoreDivXPath);
        for (var i = 0; i < readMoreDivs.length; i++) {
            readMoreDivs[i].click()
        }

        await new Promise(r => setTimeout(r, 500));
        if (readMoreDivs.length === 0) {
            loadingCommentsBuffer--;
        } else {
            loadingCommentsBuffer = 5;
        }
        if (((previousbuffer == 5) & (loadingCommentsBuffer == 4))|((previousbuffer == 4) & (loadingCommentsBuffer == 5))){
            iteration45 ++;  
            console.log('Buffer 4-5 Loop counter: '+ iteration45);          
        }
        console.log('Buffer ' + loadingCommentsBuffer);
        if(iteration45>100){
            break
        }
    }
    console.log('Openened all 2nd level comments');


    // Reading all comments, extracting and converting the data to csv
    var comments = getAllComments();

    var publisherProfileUrl = getElementsByXPath(publisherProfileUrlXPath)[0]['href'].split('?')[0];
    var nicknameAndPublishedAgoTime = getElementsByXPath(nicknameAndTimePublishedAgoXPath)[0].outerText.replaceAll('\n', ' ').split(' · ');
    var level2commentsLength = getElementsByXPath(level2CommentsXPath).length;

    var commentNumberDifference = Math.abs(getElementsByXPath(tiktokNumberOfCommentsXPath)[0].outerText - (comments.length + level2commentsLength))


    var csv = 'Comment Number (ID),Nickname,User @,User URL,Comment Text,Time,Likes,Profile Picture URL,Is 2nd Level Comment,Replied To,Number of replies\n';
    var count = 1;

    try{
        for (var i = 0; i < comments.length; i++) {
            level1comment = comments[i].children[0]
            more = comments[i].children[1]
            if (typeof more !== 'undefined') {
                more = [...more.children].slice(0,-1);
                numberOfReplies = more.length;
            } else {
                numberOfReplies = 0;
            }
            csv += count + ',' + csvFromComment(level1comment) + ',No,---,' + numberOfReplies + '\n';
            repliedTo = getElementsByXPath('./a', level1comment)[0]['href'].split('?')[0].split('/')[3].slice(1);
            for (j = 0; j < numberOfReplies; j++) {
                count++;
                csv += count + ',' + csvFromComment(more[j]) + ',Yes,' + repliedTo + ',---\n';
            }
            count++;
        }
        var apparentCommentNumber = getElementsByXPath(tiktokNumberOfCommentsXPath)[0].outerText;
        console.log('Number of magically missing comments (not rendered in the comment section): ' + (apparentCommentNumber - count + 1) + ' (you have ' + (count - 1) + ' of ' + apparentCommentNumber + ')');
        //console.log('CSV copied to clipboard!');
    
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(csv);  
        /* Alert the copied text */
        alert("Copied the text toclipboard");   
    
        await sleep(3000)
    }
    catch(e){
        console.log('---------ERROR---------');
        console.log(e);

        navigator.clipboard.writeText(e);  
        /* Alert the copied text */
        alert("Copied Error to clipboard"); 
        await sleep(3000)
    } 

    


})()
