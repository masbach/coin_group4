# Coin Seminar - Team 4 - Authenticity of Corporate Influencers



## Welcome

Welcome to the GitLab Repository of Team 4 in 2022 Collaborative Innovation Networks Seminar. In this repository you will find all relevant code snippets, analytics and data used in evaluating authenticity of corporate influencers.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/masbach/coin_group4.git
git branch -M main
git push -uf origin main
```

# 01_Download

[01_Download](https://gitlab.com/masbach/coin_group4/-/tree/main/01_Download) consists of code based on the unofficial TikTok API (Detailed Information can be found in the Scraper Notebook) in order to fetch data from TikTok. This inclueds video files, comments and video statistics such as likes, shares and Views. 

# 02 Analysis

[02_Analysis](https://gitlab.com/masbach/coin_group4/-/tree/main/02_Analysis) consists of code to evaluate authenticity of Corporate Influencers. Main features included are facial expression recognition, Comment analysis and calculations to measure the authenticity and give the evaluated influencers a ranking. This is based on a framework we developed which can be accesed in [03_Documentation](https://gitlab.com/masbach/coin_group4/-/tree/main/03_Documentation) and is describeb in the final paper.

# 03_Documentation

[03_Documentation](https://gitlab.com/masbach/coin_group4/-/tree/main/03_Documentation) consists of all documentation made for the project itself. This includes the above mentioned framework, status presentations as well as the final paper.

# 04 Google Drive Folder
GitLab contains a version of the Notebook from the analysis of the results. The complete folder can be found here: https://drive.google.com/drive/folders/1JTx0SqsByNHvwWtfmi1OqsXsPfPdlhSg?usp=sharing
Access is granted to:
pgloor@mit.edu; theresa.henn@uni-bamberg.de; Melsbach@wim.uni-koeln.de; dwamig@smail.uni-koeln.de; pgrewe1@smail.uni-koeln.de; masbach1@smail.uni-koeln.de
If someone else needs excess, please let us know under marenforrer@hotmail.com

# Authors and acknowledgment

Domenic Wamig - Universität zu Köln  
Pascal Grewe - Universität zu Köln  
Mark Asbach - Universität zu Köln  
Maren Forrer - Hochschule Luzern  


## Project status

As of the conclusion of the Coin Seminar the project stopped further development on August 5th.
